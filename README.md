# study_cas

学习 cas 单点登录 [casdoor 官网](https://casdoor.org/zh/docs/overview)

## 介绍

## 软件架构

软件架构说明

## 安装教程

### 部署 casdoor

```sh
# 默认: admin/123
docker run -itd --name casdoor -p 8000:8000 casbin/casdoor:v1.777.0
```

## 使用说明

### casdoor 服务器配置

[http://localhost:8000]

1. "证书"添加
   1. 名称: cert_haifengat
   2. 下载证书: token_jwt_key.pem 保存到程序目录 config 下
2. "组织"菜单中添加组织
   1. 名称: org_haifengat
   2. 万能密码: 12345
   3. 软删除
3. "角色"菜单中添加角色
   1. 选择组织: org_haifengat
   2. 名称: role_normal
   3. 显示名称: 普通成员
4. "模型"添加
   1. 组织: org_haifengat
   2. 名称: model_haifengat
   3. 模型文本: (RBAC)

```ini
  # 请求
  [request_definition]
  r = sub, obj, act
  # 策略
  [policy_definition]
  p = sub, obj, act
  # 角色
  [role_definition]
  g = _, _
  # 策略效果
  [policy_effect]
  e = some(where (p.eft == allow))
  # 匹配器
  [matchers]
  m = g(r.sub, p.sub)&& r.obj == p.obj && r.act == p.act
```

5. "应用"添加
   1. 名称: app_test01
   2. 选择组织: org_haifengat
   3. 得到
      1. clientid: cd3518599975d2e97f5c
      2. secret: 16f49d8622bf8159e64144351a1a5a8454f7dff5
   4. 选择证书: cert_haifengat
   5. 添加重定向 URLs:
      1. 登录页面 url 中的 redict_uri 必须在此配置中
         1. <http://localhost:9000/api/signin> 测试本地后端 api
      2. 测试 api 时填后端地址
      3. 正常流程: 前端通过 redict_uri 得到 code&state, 再向后端请求登录
   6. accessToken 过期时间 168h=7d
   7. [x] 保持登录会话
   8. [x] 启用自动登录
   9. [ ] 启用验证码登录
   10. [ ] 启用 WebAuthn 登录
   11. OAuth 授权类型
       - [x] Authorization Code
       - [x] Token
       - [ ] Password
       - [ ] ID Token
   12. 保存
   13. 测试用户注册 [http://localhost:8000/signup/app_test01]
       1. test001/123456
       2. 登录[http://localhost:8000/login/org_haifengat]
6. "权限"添加
   1. 选择组织: org_haifengat
   2. 名称: perm_normal
   3. 显示名称:低级权限
   4. 模型: model_haifengat
   5. 包含用户: org_haifengat/\* **手写**
   6. 包含角色: org_home/role_normal 模型中采用 RBAC
   7. 资源类型: 应用
   8. 资源: app_test01
   9. 动作: 读权限 写权限
   10. 状态: 审批通过
7. "用户"菜单添加
   1. 选择组织: org_haifengat
   2. 名称: haifengat
   3. 显示名称: 海风
   4. 用户类型: normal-user
   5. 密码: 123456
   6. 注册应用: app_test01
   7. [ ] 管理员
   8. [ ] 全局管理员
8. 获取的数据

   | Name (in order)  | Must | Description                                         |
   | ---------------- | ---- | --------------------------------------------------- |
   | endpoint         | Yes  | Casdoor server URL, such as <http://localhost:8000> |
   | clientId         | Yes  | Application.clientId                                |
   | clientSecret     | Yes  | Application.clientSecret                            |
   | certificate      | Yes  | x509 certificate content of Application.cert        |
   | organizationName | Yes  | Application.organization                            |
   | applicationName  | Yes  | Application.applicationName                         |

### 登录

[参考代码](https://github.com/casbin/casnode/blob/master/controllers/account.go)

1. 获取 code

   ```sh
   export code=$(echo $(curl -X POST -H 'Content-Type:application/json' \
   'http://localhost:8000/api/login?clientId=cd3518599975d2e97f5c&responseType=code&redirectUri=http%3A%2F%2Flocalhost%3A9000%2Fapi%2Fsignin&scope=read&state=app_test01' \
   -d '{
      "application": "app_test01",
      "organization": "org_haifengat",
      "username": "hf001",
      "password": "123456",
      "autoSignin": true,
      "type": "code"
   }') | awk -F ',' '{print $5}' |awk -F ':' '{print $2}'|awk -F '"' '{print $2}')
   echo $code
   ```

2. 获取 accessCode

   ```sh
   export accessToken=$(echo $(curl http://localhost:9000/api/signin?code=$code&state=app_test01) |grep -Pos "\"accessToken\":\"[^,]*\""|awk -F '"' '{print $4}')
   echo $accessToken
   ```

3. 测试

   ```sh

   ```

### casbin 权限

[rbac_model.conf](config/rbac_model.conf)

```go
var (
   dmDSN     string = "dm://SYSDBA:SYSDBA001@localhost:5236"
   modelFile string = "./config/rbac_model.conf"
)
if err := middleware.InitCasAdapter(dmDSN, modelFile); err != nil {
   logrus.Error(err)
   os.Exit(1)
}
 r.Use(middleware.CheckRole)
```

## 附

[RBAC api](https://casbin.org/zh/docs/rbac-api)

[apiFox 测试用例](https://www.apifox.cn/apidoc/shared-4a9dcf33-a2b6-4702-a20b-e96ef8d07f60)

[导入用户(xlsx 示例)](xlsx/users.xlsx)

[示例代码](demo/main.go)
