package cas

import (
	"context"
	"fmt"
	"sync"

	"github.com/casbin/casbin/v2"
	"github.com/casbin/casbin/v2/model"
)

// CasbinService 封装了 Casbin 的常用操作
type CasbinService struct {
	enforcer *casbin.Enforcer
}

var (
	casbinService *CasbinService
	once          sync.Once
)

// InitializeCasbinService 初始化 CasbinService 单实例
func InitializeCasbinService(ctxDao context.Context) error {
	var err error
	once.Do(func() {
		casbinService, err = newCasbinService(ctxDao)
	})
	return err
}

// newCasbinService 创建一个新的 CasbinService 实例
func newCasbinService(ctxDao context.Context) (*CasbinService, error) {
	cs := &CasbinService{}

	// 初始化适配器
	a, err := NewAdapter(ctxDao)
	if err != nil {
		return nil, fmt.Errorf("初始化适配器: %w", err)
	}
	// 创建一个模型
	m, err := model.NewModelFromString(`
# 请求定义:用户,应用,路径,操作
[request_definition]
r = user, obj_app, obj_url, action

# 策略定义:角色,应用(正则),路径(正则),操作(正则)
[policy_definition]
p = role, obj_app, obj_url, action

# 用户 & 角色
[role_definition]
g = _, _

[policy_effect]
e = some(where (p.eft == allow))

[matchers]
# keyMatch 匹配路径  /user/*  keyMatch2 匹配路径带参数 /usr/:id
m = r.user == 'admin' || (g(r.user, p.role) && keyMatch2(r.obj_app, p.obj_app) && keyMatch2(r.obj_url, p.obj_url) && keyMatch2(r.action, p.action))
`)
	if err != nil {
		return nil, fmt.Errorf("创建模型: %w", err)
	}

	// 初始化 enforcer
	enforcer, err := casbin.NewEnforcer(m, a)
	if err != nil {
		return nil, fmt.Errorf("初始化 enforcer: %w", err)
	}

	enforcer.EnableAutoSave(true)
	enforcer.LoadPolicy()
	cs.enforcer = enforcer

	return cs, nil
}

// GetCasbinService 获取单实例的 CasbinService, 必须先调用 InitializeCasbinService 进行初始化
func GetCasbinService() *CasbinService {
	if casbinService == nil {
		panic("CasbinService 未初始化，请先调用 InitializeCasbinService 进行初始化")
	}
	return casbinService
}

// AddPolicy 添加授权规则
func (cs *CasbinService) AddPolicy(role, app, url, action string) (bool, error) {
	b, err := cs.enforcer.AddPolicy(role, app, url, action)
	if err != nil {
		cs.enforcer.LoadPolicy()
	}
	return b, err
}

// RemovePolicy 删除授权规则
func (cs *CasbinService) RemovePolicy(role, app, url, action string) (bool, error) {
	b, err := cs.enforcer.RemovePolicy(role, app, url, action)
	if err != nil {
		cs.enforcer.LoadPolicy()
	}
	return b, err
}

// UpdatePolicy 更新授权规则
func (cs *CasbinService) UpdatePolicy(role, oldApp, oldURL, oldAction, newApp, newURL, newAction string) (bool, error) {
	b, err := cs.enforcer.UpdatePolicy([]string{role, oldApp, oldURL, oldAction}, []string{role, newApp, newURL, newAction})
	if err != nil {
		cs.enforcer.LoadPolicy()
	}
	return b, err
}

// AddUserToRole 添加用户到角色
func (cs *CasbinService) AddUserToRole(user, role string) (bool, error) {
	b, err := cs.enforcer.AddGroupingPolicy(user, role)
	if err != nil {
		cs.enforcer.LoadPolicy()
	}
	return b, err
}

// RemoveUserFromRole 删除用户角色关系
func (cs *CasbinService) RemoveUserFromRole(user, role string) (bool, error) {
	b, err := cs.enforcer.RemoveGroupingPolicy(user, role)
	if err != nil {
		cs.enforcer.LoadPolicy()
	}
	return b, err
}

// GetRolesForUser 获取用户的角色
func (cs *CasbinService) GetRolesForUser(user string) ([]string, error) {
	return cs.enforcer.GetRolesForUser(user)
}

// GetUsersForRole 获取角色的用户
func (cs *CasbinService) GetUsersForRole(role string) ([]string, error) {
	return cs.enforcer.GetUsersForRole(role)
}

// Enforce 检查权限
func (cs *CasbinService) Enforce(user, app, url, action string) (bool, error) {
	return cs.enforcer.Enforce(user, app, url, action)
}

// GetPolicy 获取所有策略
func (cs *CasbinService) GetPolicy() ([][]string, error) {
	return cs.enforcer.GetPolicy()
}

// GetGroupingPolicy 获取所有分组策略
func (cs *CasbinService) GetGroupingPolicy() ([][]string, error) {
	return cs.enforcer.GetGroupingPolicy()
}
