package cas

import (
	"context"
	"fmt"
	"testing"

	"gitee.com/chunanyong/zorm"
	zd "gitee.com/haifengat/zorm-dm/v2"
	"github.com/stretchr/testify/assert"
)

func TestCasbinService(t *testing.T) {
	zorm.FuncPrintSQL = func(ctx context.Context, sqlstr string, args []interface{}, execSQLMillis int64) {}
	dmDSN := "dm://SYSDBA:SYSDBA_dm001@localhost:5236"
	ctx, err := zd.InitDaoDM(dmDSN, "casbin")
	if err != nil {
		fmt.Println(err)
		return
	}

	err = InitializeCasbinService(ctx)
	assert.NoError(t, err)
	cs := GetCasbinService()

	t.Run("AddPolicy", func(t *testing.T) {
		ok, err := cs.AddPolicy("role1", "app1", "/data1", "read")
		assert.NoError(t, err)
		assert.True(t, ok)
	})

	t.Run("RemovePolicy", func(t *testing.T) {
		ok, err := cs.RemovePolicy("role1", "app1", "/data1", "read")
		assert.NoError(t, err)
		assert.True(t, ok)
	})

	t.Run("UpdatePolicy", func(t *testing.T) {
		_, _ = cs.AddPolicy("role1", "app1", "/data1", "read")
		ok, err := cs.UpdatePolicy("role1", "app1", "/data1", "read", "app2", "/data2", "write")
		assert.NoError(t, err)
		assert.True(t, ok)
	})

	t.Run("AddUserToRole", func(t *testing.T) {
		ok, err := cs.AddUserToRole("user1", "role1")
		assert.NoError(t, err)
		assert.True(t, ok)
	})

	t.Run("RemoveUserFromRole", func(t *testing.T) {
		_, _ = cs.AddUserToRole("user1", "role1")
		ok, err := cs.RemoveUserFromRole("user1", "role1")
		assert.NoError(t, err)
		assert.True(t, ok)
	})

	t.Run("GetRolesForUser", func(t *testing.T) {
		_, _ = cs.AddUserToRole("user1", "role1")
		roles, err := cs.GetRolesForUser("user1")
		assert.NoError(t, err)
		assert.Contains(t, roles, "role1")
	})

	t.Run("GetUsersForRole", func(t *testing.T) {
		_, _ = cs.AddUserToRole("user1", "role1")
		users, err := cs.GetUsersForRole("role1")
		assert.NoError(t, err)
		assert.Contains(t, users, "user1")
	})

	t.Run("Enforce", func(t *testing.T) {
		_, _ = cs.AddPolicy("admin", "*", "*", "*")
		_, _ = cs.AddUserToRole("admin", "admin")
		ok, err := cs.Enforce("admin", "it-manager", "/api/addPolicy", "POST")
		// _, _ = cs.AddPolicy("mana", "it-manager", "/a/*", "GET|PUT")
		// _, _ = cs.AddUserToRole("mm", "mana")
		// ok, err := cs.Enforce("mm", "it-manager", "/a/c", "GET")
		assert.NoError(t, err)
		assert.True(t, ok)
	})

	t.Run("GetPolicy", func(t *testing.T) {
		_, _ = cs.AddPolicy("role1", "app1", "/data1", "read")
		policies, err := cs.GetPolicy()
		assert.NoError(t, err)
		assert.NotEmpty(t, policies)
		fmt.Println(policies)
	})

	t.Run("GetGroupingPolicy", func(t *testing.T) {
		_, _ = cs.AddUserToRole("user1", "role1")
		groupingPolicies, err := cs.GetGroupingPolicy()
		assert.NoError(t, err)
		assert.NotEmpty(t, groupingPolicies)
		fmt.Println(groupingPolicies)
	})
}
