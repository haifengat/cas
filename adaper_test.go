package cas

import (
	"context"
	"fmt"
	"testing"

	"gitee.com/chunanyong/zorm"
	zd "gitee.com/haifengat/zorm-dm/v2"
	"github.com/casbin/casbin/v2"
	"github.com/casbin/casbin/v2/model"
)

// TestWithDomsField 测试 Casbin enforcer 的功能，使用自定义模型和适配器。
// 它执行以下步骤：
// 1. 初始化数据库连接和上下文。
// 2. 创建一个新的适配器。
// 3. 从字符串定义创建一个新的模型。
// 4. 使用模型和适配器创建一个新的 enforcer。
// 5. 启用 enforcer 的自动保存和日志记录。
// 6. 定义用户、角色、应用、部门、URL 和操作变量进行测试。
// 7. 向 enforcer 添加策略。
// 8. 检索并打印当前策略。
// 9. 向 enforcer 添加分组策略（用户-角色关系）。
// 10. 使用 enforcer 检查权限并打印结果。

// 适配器测试
func TestWithDomsField(t *testing.T) {
	zorm.FuncPrintSQL = func(ctx context.Context, sqlstr string, args []interface{}, execSQLMillis int64) {}

	dmDSN := "dm://SYSDBA:SYSDBA_dm001@localhost:5236"
	ctxDao, err := zd.InitDaoDM(dmDSN, "casbin")
	if err != nil {
		fmt.Println(err)
		return
	}
	var b bool

	// 1. 创建一个适配器
	a, err := NewAdapter(ctxDao)
	if err != nil {
		fmt.Println("创建适配器: ", err)
		return
	}

	// 2. 创建一个模型
	m, err := model.NewModelFromString(`
# 部门采用 部门间用 / 分割的方式
# 请求定义:用户,应用,路径,部门,操作
[request_definition]
r = user, obj_app, obj_url, dom_department, action

# 策略定义:角色,应用,路径,部门,操作
# 部门间用 / 分割, 最后加上 /* 以表示所有子部门的权限
[policy_definition]
# dom_department(部门)的role(岗)对obj_app(应用)的obj_url(路径)有action(操作)的权限
# dom_department 可用 | 分割多个部门
p = role, obj_app, obj_url, dom_department, action

# 用户 & 角色关系: 用户,角色,应用,部门
[role_definition]
g = _, _, _, _

[policy_effect]
e = some(where (p.eft == allow))

[matchers]
# keyMatch 匹配路径  /user/*  keyMatch2 匹配路径带参数 /usr/:id
# 注: keyMatch2 和 keyMatch 不能同时使用, 参数为 (r. p.) 与 regexMatch 相反
m = g(r.user, p.role, r.obj_app, r.dom_department) && r.obj_app == p.obj_app && keyMatch2(r.dom_department, p.dom_department) && keyMatch2(r.obj_url, p.obj_url) && regexMatch(p.action, r.action)
`)
	if err != nil {
		fmt.Println("创建模型: ", err)
		return
	}

	// 3. 创建一个Enforcer
	enforcer, err := casbin.NewEnforcer(m, a)
	if err != nil {
		fmt.Println("创建Enforcer: ", err)
		return
	}

	enforcer.EnableAutoSave(true) // 启用自动保存
	enforcer.EnableLog(true)      // 启用日志

	var user, role, app, department, url, action string
	user = "alice"
	role = "manager"
	app = "app"
	department = "dept/*"
	url = "/user/*"
	action = "GET|POST"

	objTest := "/user/ab/c"    // 测试正则的 object
	deptTest := "dept/subDept" // 测试正则的 department

	role2 := "admin"
	url2 := "/path/:id"
	objTest2 := "/path/test"

	// 添加策略 角色,应用,路径,部门,操作
	b, err = enforcer.AddPolicy(role, app, url, department, action) // 添加角色:权限关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加策略:", b)
	b, err = enforcer.AddPolicy(role, app, url2, department, action) // 添加角色:权限关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加策略:", b)
	b, err = enforcer.AddPolicy(role2, app, url2, department, action) // 添加角色:权限关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加策略2:", b)

	ps, err := enforcer.GetPolicy() // 获取策略
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("策略:", ps)

	// model := enforcer.GetModel()
	// fmt.Printf("model: %+v\n", model)

	// 用户,角色,应用,部门
	b, err = enforcer.AddGroupingPolicy(user, role, app, department) // 添加角色关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加角色关系:", b)
	b, err = enforcer.AddGroupingPolicy(user, role2, app, department) // 添加角色关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加角色关系:", b)

	// 请求定义:用户,应用,路径,部门,操作
	b, err = enforcer.Enforce(user, app, url, deptTest, action) // 检查权限
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("检查权限:", b)

	b, err = enforcer.Enforce(user, app, objTest, department, "POST") // 检查权限
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("检查权限(正则obj+act):", b)

	b, err = enforcer.Enforce(user, app, objTest2, department, "POST") // 检查权限
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("检查权限(正则act):", b)
}

// 多级部门测试
func TestWithDomsRegexMultiDepartment(t *testing.T) {
	zorm.FuncPrintSQL = func(ctx context.Context, sqlstr string, args []interface{}, execSQLMillis int64) {}

	dmDSN := "dm://SYSDBA:SYSDBA_dm001@localhost:5236"
	ctxDao, err := zd.InitDaoDM(dmDSN, "casbin")
	if err != nil {
		fmt.Println(err)
		return
	}
	var b bool

	// 1. 创建一个适配器
	a, err := NewAdapter(ctxDao)
	if err != nil {
		fmt.Println("创建适配器: ", err)
		return
	}

	// 2. 创建一个模型
	m, err := model.NewModelFromString(`
# 部门采用 部门间用 / 分割的方式
# 请求定义:用户,应用,路径,部门,操作
[request_definition]
r = user, app, url, department, action # 请求定义:用户,应用,路径,部门,操作

# 策略定义:角色,应用,路径,部门,操作
# 部门间用 / 分割, 最后加上 /* 以表示所有子部门的权限
[policy_definition]
p = role, app, url, department, action

# 用户 & 角色关系: 用户,角色,应用,部门
[role_definition]
g = _, _, _, _

[policy_effect]
e = some(where (p.eft == allow))

[matchers]
# keyMatch 匹配路径  /user/*  keyMatch2 匹配路径带参数 /usr/:id
# 注: keyMatch2 和 keyMatch 不能同时使用, 参数为 (r. p.) 与 regexMatch 相反
m = g(r.user, p.role, r.app, r.department) && r.app == p.app && keyMatch2(r.department, p.department) && keyMatch2(r.url, p.url) && regexMatch(p.action, r.action)
`)
	if err != nil {
		fmt.Println("创建模型: ", err)
		return
	}

	// 3. 创建一个Enforcer
	enforcer, err := casbin.NewEnforcer(m, a)
	if err != nil {
		fmt.Println("创建Enforcer: ", err)
		return
	}

	enforcer.EnableAutoSave(true) // 启用自动保存
	enforcer.EnableLog(true)      // 启用日志

	var user, role, app, department, url, action string
	user = "alice"
	role = "manager"
	app = "app"
	department = "dept/*"
	url = "/user/*"
	action = "GET|POST"

	objTest := "/user/ab/c"    // 测试正则的 object
	deptTest := "dept/subDept" // 测试正则的 department

	role2 := "admin"
	url2 := "/path/:id"
	objTest2 := "/path/test"

	// 添加策略 角色,应用,路径,部门,操作
	b, err = enforcer.AddPolicy(role, app, url, department, action) // 添加角色:权限关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加策略:", b)
	b, err = enforcer.AddPolicy(role2, app, url2, department, action) // 添加角色:权限关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加策略2:", b)

	ps, err := enforcer.GetPolicy() // 获取策略
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("策略:", ps)

	// model := enforcer.GetModel()
	// fmt.Printf("model: %+v\n", model)

	// 用户,角色,应用,部门
	b, err = enforcer.AddGroupingPolicy(user, role, app, department) // 添加角色关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加角色关系:", b)
	b, err = enforcer.AddGroupingPolicy(user, role2, app, department) // 添加角色关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加角色关系:", b)

	// 请求定义:用户,应用,路径,部门,操作
	b, err = enforcer.Enforce(user, app, url, deptTest, action) // 检查权限
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("检查权限:", b)

	b, err = enforcer.Enforce(user, app, objTest, department, "POST") // 检查权限
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("检查权限(正则obj+act):", b)

	b, err = enforcer.Enforce(user, app, objTest2, department, "POST") // 检查权限
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("检查权限(正则act):", b)
}

// 多角色测试
func TestWithDomsRegexMultiRole(t *testing.T) {
	zorm.FuncPrintSQL = func(ctx context.Context, sqlstr string, args []interface{}, execSQLMillis int64) {}

	dmDSN := "dm://SYSDBA:SYSDBA_dm001@localhost:5236"
	ctxDao, err := zd.InitDaoDM(dmDSN, "casbin")
	if err != nil {
		fmt.Println(err)
		return
	}
	var b bool

	// 1. 创建一个适配器
	a, err := NewAdapter(ctxDao)
	if err != nil {
		fmt.Println("创建适配器: ", err)
		return
	}

	// 2. 创建一个模型
	m, err := model.NewModelFromString(`
[request_definition]
r = user, object, dom1, dom2, action # 请求定义:用户、对象、域1、域2、动作

[policy_definition]
p = role, object, dom1, dom2, action

[role_definition]
g = _, _, _, _ # user, role, dom1, dom2

[policy_effect]
e = some(where (p.eft == allow))

[matchers]
# keyMatch 匹配路径  /user/*  keyMatch2 匹配路径带参数 /usr/:id
# 注: keyMatch2 和 keyMatch 不能同时使用, 参数为 (r. p.) 与 regexMatch 相反
m = g(r.user, p.role, r.dom1, r.dom2) && r.dom1 == p.dom1 && r.dom2 == p.dom2 && keyMatch2(r.object, p.object) && regexMatch(p.action, r.action)
`)
	if err != nil {
		fmt.Println("创建模型: ", err)
		return
	}

	// 3. 创建一个Enforcer
	enforcer, err := casbin.NewEnforcer(m, a)
	if err != nil {
		fmt.Println("创建Enforcer: ", err)
		return
	}

	enforcer.EnableAutoSave(true) // 启用自动保存
	enforcer.EnableLog(true)      // 启用日志

	var user, role, dom1, dom2, obj, action string
	user = "alice"
	role = "manager"
	dom1 = "app"
	dom2 = "dept"
	obj = "/user/*"
	action = "GET|POST"

	role2 := "admin"
	obj2 := "/dept/:id"

	objTest := "/user/ab/c" // 测试正则的 object

	// 添加策略
	b, err = enforcer.AddPolicy(role, obj, dom1, dom2, action) // 添加角色:权限关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加策略:", b)
	b, err = enforcer.AddPolicy(role2, obj2, dom1, dom2, action) // 添加角色:权限关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加策略2:", b)

	ps, err := enforcer.GetPolicy() // 获取策略
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("策略:", ps)

	// model := enforcer.GetModel()
	// fmt.Printf("model: %+v\n", model)

	b, err = enforcer.AddGroupingPolicy(user, role, dom1, dom2) // 添加角色关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加角色关系:", b)
	b, err = enforcer.AddGroupingPolicy(user, role2, dom1, dom2) // 添加角色关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加角色关系:", b)

	b, err = enforcer.Enforce(user, obj, dom1, dom2, action) // 检查权限
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("检查权限:", b)

	b, err = enforcer.Enforce(user, objTest, dom1, dom2, "POST") // 检查权限
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("检查权限(正则obj+act):", b)

	b, err = enforcer.Enforce(user, "/dept/3", dom1, dom2, "POST") // 检查权限
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("检查权限(正则act):", b)
}

// 正则匹配测试
func TestWithDomsRegex(t *testing.T) {
	zorm.FuncPrintSQL = func(ctx context.Context, sqlstr string, args []interface{}, execSQLMillis int64) {}

	dmDSN := "dm://SYSDBA:SYSDBA_dm001@localhost:5236"
	ctxDao, err := zd.InitDaoDM(dmDSN, "casbin")
	if err != nil {
		fmt.Println(err)
		return
	}
	var b bool

	// 1. 创建一个适配器
	a, err := NewAdapter(ctxDao)
	if err != nil {
		fmt.Println("创建适配器: ", err)
		return
	}

	// 2. 创建一个模型
	m, err := model.NewModelFromString(`
[request_definition]
r = user, object, dom1, dom2, action # 请求定义:用户、对象、域1、域2、动作

[policy_definition]
p = role, object, dom1, dom2, action

[role_definition]
g = _, _, _, _ # user, role, dom1, dom2

[policy_effect]
e = some(where (p.eft == allow))

[matchers]
# keyMatch 匹配路径  /user/*
m = g(r.user, p.role, r.dom1, r.dom2) && r.dom1 == p.dom1 && r.dom2 == p.dom2 && keyMatch(r.object, p.object) && regexMatch(p.action, r.action)
`)
	if err != nil {
		fmt.Println("创建模型: ", err)
		return
	}

	// 3. 创建一个Enforcer
	enforcer, err := casbin.NewEnforcer(m, a)
	if err != nil {
		fmt.Println("创建Enforcer: ", err)
		return
	}

	enforcer.EnableAutoSave(true) // 启用自动保存
	enforcer.EnableLog(true)      // 启用日志

	var user, role, dom1, dom2, obj, action string
	user = "alice"
	role = "manager"
	dom1 = "app"
	dom2 = "dept"
	obj = "/user/*"
	action = "GET|POST"

	// keyMatch 匹配路径  /user/*
	// keyMatch2 匹配路径带参数 /usr/:id
	// 注: keyMatch2 和 keyMatch 不能同时使用, 参数为 (r. p.) 与 regexMatch 相反
	objTest := "/user/ab/c" // 测试正则的 object

	// 添加策略
	b, err = enforcer.AddPolicy(role, obj, dom1, dom2, action) // 添加角色:权限关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加策略:", b)

	ps, err := enforcer.GetPolicy() // 获取策略
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("策略:", ps)

	// model := enforcer.GetModel()
	// fmt.Printf("model: %+v\n", model)

	b, err = enforcer.AddGroupingPolicy(user, role, dom1, dom2) // 添加角色关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加角色关系:", b)

	roles, err := enforcer.GetRolesForUser(user, dom1, dom2)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("获取角色:", roles)

	users, err := enforcer.GetUsersForRole(role, dom1, dom2)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("获取用户:", users)

	// 部分检测
	// b1, err := regexp.MatchString("/user/.*", "/user/abc")
	// fmt.Println(b1, err)

	b, err = enforcer.Enforce(user, obj, dom1, dom2, action) // 检查权限
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("检查权限:", b)

	b, err = enforcer.Enforce(user, obj, dom1, dom2, "POST") // 检查权限
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("检查权限(正则act):", b)

	b, err = enforcer.Enforce(user, objTest, dom1, dom2, "POST") // 检查权限
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("检查权限(正则obj+act):", b)

	b, err = enforcer.RemovePolicy(role, obj, dom1, dom2, action) // 删除策略
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("删除策略:", b)

	// enforcer.UpdatePolicy([]string{role, old, oldMethod}, []string{roleName, newPath, newMethod})

	b, err = enforcer.RemoveGroupingPolicy(user, role, dom1, dom2) // 删除角色关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("删除角色关系:", b)
}

// 用户在某个应用的某个部门有某个对象的权限
func TestWithDom2(t *testing.T) {
	zorm.FuncPrintSQL = func(ctx context.Context, sqlstr string, args []interface{}, execSQLMillis int64) {}

	dmDSN := "dm://SYSDBA:SYSDBA_dm001@localhost:5236"
	ctxDao, err := zd.InitDaoDM(dmDSN, "casbin")
	if err != nil {
		fmt.Println(err)
		return
	}
	var b bool

	// 1. 创建一个适配器
	a, err := NewAdapter(ctxDao)
	if err != nil {
		fmt.Println("创建适配器: ", err)
		return
	}

	// 2. 创建一个模型
	m, err := model.NewModelFromString(`[request_definition]
r = user, object, dom1, dom2, action # 请求定义:用户、对象、域1、域2、动作

[policy_definition]
p = role, object, dom1, dom2, action

[role_definition]
g = _, _, _, _ # user, role, dom1, dom2

[policy_effect]
e = some(where (p.eft == allow))

[matchers]
m = g(r.user, p.role, r.dom1, r.dom2) && r.dom1 == p.dom1 && r.dom2 == p.dom2 && r.object == p.object && r.action == p.action
`)
	if err != nil {
		fmt.Println("创建模型: ", err)
		return
	}

	// 3. 创建一个Enforcer
	enforcer, err := casbin.NewEnforcer(m, a)
	if err != nil {
		fmt.Println("创建Enforcer: ", err)
		return
	}

	enforcer.EnableAutoSave(true) // 启用自动保存
	// enforcer.EnableLog(true)      // 启用日志

	var user, role, dom1, dom2, obj, action string
	user = "alice"
	role = "manager"
	dom1 = "app"
	dom2 = "dept"
	obj = "data1"
	action = "read"

	// 添加策略
	b, err = enforcer.AddPolicy(role, obj, dom1, dom2, action) // 添加角色:权限关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加策略:", b)

	ps, err := enforcer.GetPolicy() // 获取策略
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("策略:", ps)

	// model := enforcer.GetModel()
	// fmt.Printf("model: %+v\n", model)

	b, err = enforcer.AddGroupingPolicy(user, role, dom1, dom2) // 添加角色关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加角色关系:", b)

	roles, err := enforcer.GetRolesForUser(user, dom1, dom2)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("获取角色:", roles)

	users, err := enforcer.GetUsersForRole(role, dom1, dom2)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("获取用户:", users)

	b, err = enforcer.Enforce(user, obj, dom1, dom2, action) // 检查权限
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("检查权限:", b)

	b, err = enforcer.Enforce(user, obj, "dom_other", "", action) // 检查权限
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("检查权限:", b)

	b, err = enforcer.RemovePolicy(role, obj, dom1, dom2, action) // 删除策略
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("删除策略:", b)

	// enforcer.UpdatePolicy([]string{role, old, oldMethod}, []string{roleName, newPath, newMethod})

	b, err = enforcer.RemoveGroupingPolicy(user, role, dom1, dom2) // 删除角色关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("删除角色关系:", b)
}

// 用户在某个应用有某个对象的权限
func TestWithDom(t *testing.T) {
	zorm.FuncPrintSQL = func(ctx context.Context, sqlstr string, args []interface{}, execSQLMillis int64) {}

	dmDSN := "dm://SYSDBA:SYSDBA_dm001@localhost:5236"
	ctxDao, err := zd.InitDaoDM(dmDSN, "casbin")
	if err != nil {
		fmt.Println(err)
		return
	}
	var b bool

	// 1. 创建一个适配器
	a, err := NewAdapter(ctxDao)
	if err != nil {
		fmt.Println("创建适配器: ", err)
		return
	}

	// 2. 创建一个模型
	m, err := model.NewModelFromString(`[request_definition]
r = user, object, dom, action

[policy_definition]
p = role, object, dom, action

[role_definition]
g = _, _, _ # user, role, domain

[policy_effect]
e = some(where (p.eft == allow))

[matchers]
m = g(r.user, p.role, r.dom) && r.dom == p.dom && r.object == p.object && r.action == p.action
`)
	if err != nil {
		fmt.Println("创建模型: ", err)
		return
	}

	// 3. 创建一个Enforcer
	enforcer, err := casbin.NewEnforcer(m, a)
	if err != nil {
		fmt.Println("创建Enforcer: ", err)
		return
	}

	enforcer.EnableAutoSave(true) // 启用自动保存
	// enforcer.EnableLog(true)      // 启用日志

	var user, role, dom, obj, action string
	user = "alice"
	role = "manager"
	dom = "app"
	obj = "data1"
	action = "read"

	// 添加策略
	b, err = enforcer.AddPolicy(role, obj, dom, action) // 添加角色:权限关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加策略:", b)

	ps, err := enforcer.GetPolicy() // 获取策略
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("策略:", ps)

	// model := enforcer.GetModel()
	// fmt.Printf("model: %+v\n", model)

	b, err = enforcer.AddGroupingPolicy(user, role, dom) // 添加角色关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("添加角色关系:", b)

	roles, err := enforcer.GetRolesForUser(user, dom)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("获取角色:", roles)

	users, err := enforcer.GetUsersForRole(role, dom)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("获取用户:", users)

	b, err = enforcer.Enforce(user, obj, dom, action) // 检查权限
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("检查权限:", b)

	b, err = enforcer.Enforce(user, obj, "dom_other", action) // 检查权限
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("检查权限:", b)

	b, err = enforcer.RemovePolicy(role, obj, dom, action) // 删除策略
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("删除策略:", b)

	// enforcer.UpdatePolicy([]string{role, old, oldMethod}, []string{roleName, newPath, newMethod})

	b, err = enforcer.RemoveGroupingPolicy(user, role, dom) // 删除角色关系
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("删除角色关系:", b)
}
