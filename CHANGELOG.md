<!--
 * @Author: haifengat hubert28@qq.com
 * @Date: 2023-05-22 13:57:25
 * @LastEditors: haifengat hubert28@qq.com
 * @LastEditTime: 2023-05-31 10:34:47
 * @FilePath: /cas/CHANGELOG.md
 * @Description: 更新记录
-->
# 更新记录

## v0.0.7

- 升级: zorm-DM
- 新增: adapter, 不再依赖 cas-dm-adapter

## v0.0.6

- 更新: InitCas 参数全部改为 string
- 优化: 所有返回为 StatusOK

## v0.0.5

- 优化: 功能使用 cas 统一 package, gin.Connect 函数使用 Hdl 开头
- 删除: gin 相关中间件
- 优化: 统一 response 回复内容
- 修复: 策略管理移至 / 路径下, 不进行登录验证
- 修复: check_role 中 URL.Path 不需要加 /
- 新增: gin 中加入用户角色请求
- 新增: AddUserToRole RemoveUserFromRole GetRolesForUser GetUsersForRole

## v0.0.4

- 更新: 去除 zorm-dm 信赖
- 新增: 添加用户到角色
- 新增: 策略增删改
- 新增: changePassword
- 更新: 适配器名称改为 cas-dm-adapter

## v0.0.3

- 更新: 用 rbac API 简化代码
- 更新: 测试用例成功通过
- 修复: session 代码放在 group 中导致不能验证权限的 bug
- 更新: README 通过 curl 取 code 的方式
- 更新: cas_role 测试用例
- 新增: hfcasbin 用户加组
- 新增: hfcasbin 策略增删改查
- 新增: GetEnforcer 取得 Casbin 的 enforcer, 采用单例模式

## v0.0.2

- 新增: casbin 权限验证中间件
- 更新: InitCas 增加 jwt_key.pem 路径
- 优化: 目录结构调整
- 新增: 用户导入 xlsx 的模板 [xlsx/users.xlsx](xlsx/users.xlsx)
- 新增: 初始化 cas 参数时, viper 读取环境变量, 读取指定 yaml 文件
- 更新: README.md 加入导入用户 xlsx 模板

## v0.0.1

- 优化: cors 函数
- 优化: log reponse 限定 100 字符,避免刷屏
- 修复: 采用 memstore 保存 claim, cookie 无法传递的问题
- 新增: 读取本地 token 文件
- 新增: ResponseOK ResponseErr 统一响应格式;
- 新增: /code 回显示登录的 code state
