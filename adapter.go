package cas

import (
	"context"
	"errors"
	"strings"

	"github.com/casbin/casbin/v2/model"
	"github.com/casbin/casbin/v2/persist"

	zd "gitee.com/haifengat/zorm-dm/v2"
)

type CasbinRule struct {
	Category   string `zorm:"type:varchar(100);index;not null;comment:项目类别"`
	Sub        string `zorm:"type:varchar(100);index;not null;comment:名称"`
	V0         string `zorm:"type:varchar(100);index;not null;comment:应用"`
	V1         string `zorm:"type:varchar(100);index;comment:URL"`
	V2         string `zorm:"type:varchar(100);index;comment:操作"`
	V3         string `zorm:"type:varchar(32767);index;comment:保留字段"`
	InsertTime string `zorm:"varchar(19);index;default:to_char(sysdate,'YYYY-MM-DD HH24:MI:SS');comment:插入时间"`
	UpdateTime string `zorm:"varchar(19);index;default:to_char(sysdate,'YYYY-MM-DD HH24:MI:SS');comment:更新时间"`
	zd.Entity
}

func (c *CasbinRule) toMap() map[string]any {
	mp := map[string]any{"Category": c.Category}
	if c.Sub != "" {
		mp["Sub"] = c.Sub
	}
	if c.V0 != "" {
		mp["V0"] = c.V0
	}
	if c.V1 != "" {
		mp["V1"] = c.V1
	}
	if c.V2 != "" {
		mp["V2"] = c.V2
	}
	if c.V3 != "" {
		mp["V3"] = c.V3
	}
	return mp
}

func (c *CasbinRule) toStringPolicy() []string {
	policy := make([]string, 0)
	if c.Category != "" {
		policy = append(policy, c.Category)
	}
	if c.Sub != "" {
		policy = append(policy, c.Sub)
	}
	if c.V0 != "" {
		policy = append(policy, c.V0)
	}
	if c.V1 != "" {
		policy = append(policy, c.V1)
	}
	if c.V2 != "" {
		policy = append(policy, c.V2)
	}
	if c.V3 != "" {
		policy = append(policy, c.V3)
	}
	return policy
}

func loadPolicyLine(line CasbinRule, model model.Model) {
	var p = []string{line.Category, line.Sub, line.V0, line.V1, line.V2, line.V3}
	var lineText string
	if line.V3 != "" {
		lineText = strings.Join(p, ", ")
	} else if line.V2 != "" {
		lineText = strings.Join(p[:5], ", ")
	} else if line.V1 != "" {
		lineText = strings.Join(p[:4], ", ")
	} else if line.V0 != "" {
		lineText = strings.Join(p[:3], ", ")
	} else if line.Sub != "" {
		lineText = strings.Join(p[:2], ", ")
	}

	persist.LoadPolicyLine(lineText, model)
}

func (a *Adapter) genPolicyLine(ptype string, rule []string) CasbinRule {
	line := CasbinRule{Category: ptype}

	l := len(rule)
	if l > 0 {
		line.Sub = rule[0]
	}
	if l > 1 {
		line.V0 = rule[1]
	}
	if l > 2 {
		line.V1 = rule[2]
	}
	if l > 3 {
		line.V2 = rule[3]
	}
	if l > 4 {
		line.V3 = rule[4]
	}
	return line
}

type Adapter struct {
	isFiltered bool
	ctxDao     context.Context
}
type Filter struct {
	Category []string
	Sub      []string
	V0       []string
	V1       []string
	V2       []string
	V3       []string
}

func (f *Filter) toMap() map[string]any {
	mp := make(map[string]any)
	if len(f.Category) > 0 {
		mp["Category"] = f.Category
	}
	if len(f.Sub) > 0 {
		mp["Sub"] = f.Sub
	}
	if len(f.V0) > 0 {
		mp["V0"] = f.V0
	}
	if len(f.V1) > 0 {
		mp["V1"] = f.V1
	}
	if len(f.V2) > 0 {
		mp["V2"] = f.V2
	}
	if len(f.V3) > 0 {
		mp["V3"] = f.V3
	}
	return mp
}

// NewAdapter is the constructor for Adapter.
//
//	@param ctxDao Context of 达梦/oracle/pgsql
//	@return a *Adapter
//	@return err
func NewAdapter(ctxDao context.Context) (a *Adapter, err error) {
	a = &Adapter{}
	a.ctxDao = ctxDao

	if err := zd.CreateSchema(a.ctxDao); err != nil {
		return nil, err
	}
	if err := zd.CreateTable[CasbinRule](a.ctxDao); err != nil {
		return nil, err
	}
	// 触发器，自动更新 UpdateTime
	n, err := zd.SelectCountByName(ctxDao, "SYS.SYSOBJECTS", map[string]any{"TYPE$": "SCHOBJ", "SUBTYPE$": "TRIG", "NAME": "TRG_CASBIN_RULE_UPDATE_TIME"})
	if err != nil {
		return nil, err
	}
	if n == 0 {
		if _, err := zd.ExecuteSql(ctxDao, `CREATE OR REPLACE TRIGGER TRG_CASBIN_RULE_UPDATE_TIME
		BEFORE UPDATE ON `+zd.GetTableName[CasbinRule](ctxDao)+`
		FOR EACH ROW
		BEGIN
		    :NEW.UpdateTime := TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS');
		END`); err != nil {
			return nil, err
		}
	}
	return a, nil
}

func (a *Adapter) AddPolicy(sec string, ptype string, rule []string) (err error) {
	line := a.genPolicyLine(ptype, rule)
	_, err = zd.InsertMap[CasbinRule](a.ctxDao, line.toMap())
	return
}

func (a *Adapter) RemovePolicy(sec string, ptype string, rule []string) (err error) {
	line := a.genPolicyLine(ptype, rule)
	_, err = zd.Delete[CasbinRule](a.ctxDao, line.toMap())
	return
}

func (a *Adapter) RemoveFilteredPolicy(sec string, ptype string, fieldIndex int, fieldValues ...string) (err error) {
	where := map[string]any{"Category": ptype}
	idx := fieldIndex + len(fieldValues)
	if fieldIndex <= 0 && idx > 0 {
		where["Sub"] = fieldValues[0-fieldIndex]
	}
	if fieldIndex <= 1 && idx > 1 {
		where["V0"] = fieldValues[1-fieldIndex]
	}
	if fieldIndex <= 2 && idx > 2 {
		where["V1"] = fieldValues[2-fieldIndex]
	}
	if fieldIndex <= 3 && idx > 3 {
		where["V2"] = fieldValues[3-fieldIndex]
	}
	if fieldIndex <= 4 && idx > 4 {
		where["V3"] = fieldValues[4-fieldIndex]
	}
	_, err = zd.Delete[CasbinRule](a.ctxDao, where)
	return
}

func (a *Adapter) LoadFilteredPolicy(model model.Model, filter interface{}) (err error) {
	filterValue, ok := filter.(Filter)
	if !ok {
		err = errors.New("invalid filter type")
		return
	}

	var lines []CasbinRule
	lines, err = zd.Select[CasbinRule](a.ctxDao, nil, filterValue.toMap())
	if err != nil {
		return
	}
	for _, line := range lines {
		loadPolicyLine(line, model)
	}
	a.isFiltered = true
	return
}

func (a *Adapter) UpdatePolicy(sec string, ptype string, oldRule, newPolicy []string) (err error) {
	oRule := a.genPolicyLine(ptype, oldRule)
	nRule := a.genPolicyLine(ptype, newPolicy)
	if _, err := zd.UpdateMap[CasbinRule](a.ctxDao, oRule.toMap(), nRule.toMap()); err != nil {
		return err
	}
	return nil
}

func (a *Adapter) UpdatePolicies(sec string, ptype string, oldRules, newRules [][]string) (err error) {
	for i, oldRule := range oldRules {
		nRule, oRule := a.genPolicyLine(ptype, newRules[i]), a.genPolicyLine(ptype, oldRule)
		if _, err = zd.UpdateMap[CasbinRule](a.ctxDao, oRule.toMap(), nRule.toMap()); err != nil {
			return
		}
	}
	return
}

func (a *Adapter) UpdateFilteredPolicies(sec string, ptype string, newPolicies [][]string, fieldIndex int, fieldValues ...string) (oldPolicies [][]string, err error) {
	// UpdateFilteredPolicies deletes old rules and adds new rules.
	line := &CasbinRule{}

	line.Category = ptype
	if fieldIndex <= 0 && 0 < fieldIndex+len(fieldValues) {
		line.Sub = fieldValues[0-fieldIndex]
	}
	if fieldIndex <= 1 && 1 < fieldIndex+len(fieldValues) {
		line.V0 = fieldValues[1-fieldIndex]
	}
	if fieldIndex <= 2 && 2 < fieldIndex+len(fieldValues) {
		line.V1 = fieldValues[2-fieldIndex]
	}
	if fieldIndex <= 3 && 3 < fieldIndex+len(fieldValues) {
		line.V2 = fieldValues[3-fieldIndex]
	}
	if fieldIndex <= 4 && 4 < fieldIndex+len(fieldValues) {
		line.V3 = fieldValues[4-fieldIndex]
	}

	newP := make([]CasbinRule, 0, len(newPolicies))
	for _, newRule := range newPolicies {
		newP = append(newP, a.genPolicyLine(ptype, newRule))
	}

	var oldP []CasbinRule
	oldP, err = zd.Select[CasbinRule](a.ctxDao, nil, line.toMap())
	if err != nil {
		return
	}
	if _, err = zd.Delete[CasbinRule](a.ctxDao, line.toMap()); err != nil {
		return
	}
	_, err = zd.Insert(a.ctxDao, false, newP...)
	if err != nil {
		return
	}

	// return deleted rulues
	oldPolicies = make([][]string, 0)
	for _, v := range oldP {
		oldPolicy := v.toStringPolicy()
		oldPolicies = append(oldPolicies, oldPolicy)
	}
	return oldPolicies, nil
}

// LoadPolicy loads policy from database.
func (a *Adapter) LoadPolicy(model model.Model) error {
	if lines, err := zd.Select[CasbinRule](a.ctxDao, nil, nil); err != nil {
		return err
	} else {
		for _, line := range lines {
			loadPolicyLine(line, model)
		}
	}
	return nil
}

// SavePolicy saves policy to database.
func (a *Adapter) SavePolicy(model model.Model) (err error) {
	if err = zd.DropTable[CasbinRule](a.ctxDao); err != nil {
		return
	}
	if err = zd.CreateTable[CasbinRule](a.ctxDao); err != nil {
		return
	}

	lines := make([]CasbinRule, 0)

	for ptype, ast := range model["p"] {
		for _, rule := range ast.Policy {
			line := a.genPolicyLine(ptype, rule)
			lines = append(lines, line)
		}
	}

	for ptype, ast := range model["g"] {
		for _, rule := range ast.Policy {
			line := a.genPolicyLine(ptype, rule)
			lines = append(lines, line)
		}
	}

	// check whether the policy is empty
	if len(lines) == 0 {
		return
	}
	_, err = zd.Insert(a.ctxDao, false, lines...)
	return
}

// RemovePolicies removes multiple policy rule from the storage.
func (a *Adapter) RemovePolicies(sec string, ptype string, rules [][]string) (err error) {
	for _, rule := range rules {
		line := a.genPolicyLine(ptype, rule)
		if _, err = zd.Delete[CasbinRule](a.ctxDao, line.toMap()); err != nil {
			return
		}
	}
	return
}

// IsFiltered returns true if the loaded policy has been filtered.
func (a *Adapter) IsFiltered() bool {
	return a.isFiltered
}
