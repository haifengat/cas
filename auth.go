package cas

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"slices"
	"strings"
	"sync"

	"github.com/casdoor/casdoor-go-sdk/casdoorsdk"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/memstore"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
)

var authPool = sync.Pool{
	New: func() any {
		// 注册casdoorsdk.Claims类型，以便在后续过程中能够存储该类型的数据。
		gob.Register(casdoorsdk.Claims{})
		// 注册casdoorsdk.User类型，以便在后续过程中能够存储该类型的数据。
		gob.Register(casdoorsdk.User{})
		return &Auth{}
	},
}

type Auth struct {
	orgName, appName, endPoint, clientID string
	WhiteList                            []string // 白名单
}

// NewAuth 创建并初始化一个Auth实例，用于与Casdoor服务进行交互。
//
// 参数:
//
//	r: Gin的RouterGroup 实例，用于设置中间件和路由
//	endpoint: Casdoor服务的访问地址
//	clientID: 应用在Casdoor上的客户端ID
//	clientSecret: 应用在Casdoor上的客户端密钥
//	certificate: 用于加密的证书
//	organizationName: 组织名称
//	applicationName: 应用名称
//
// 返回值:
// - *Auth: 初始化后的Auth实例。
func NewAuth(r *gin.RouterGroup, endpoint, clientID, clientSecret, certificate, organizationName, applicationName string) *Auth {
	// 从authPool中获取一个Auth实例进行复用
	auth := authPool.Get().(*Auth)
	auth.orgName = organizationName
	auth.appName = applicationName
	auth.endPoint = endpoint
	auth.clientID = clientID

	// 初始化配置，为与Casdoor服务的交互做准备。
	casdoorsdk.InitConfig(endpoint, clientID, clientSecret, certificate, organizationName, applicationName)

	// 初始化session存储
	store := memstore.NewStore([]byte("secret-key-2025"))
	r.Use(sessions.Sessions("casdoor", store))

	// 初始化白名单，默认为空列表
	auth.WhiteList = []string{}
	return auth
}

// getCode 获取用户认证的code
//
// 参数:
//
//	userName: string 用户名
//	passWord: string 密码
//
// 返回值:
//
//	string - 认证的code
//	error - 错误信息，如果出现错误则返回
func (a *Auth) getCode(userName, passWord string) (string, error) {
	// 构建登录URL
	url := fmt.Sprintf("%s/api/login?scope=read&state=%s&clientId=%s&responseType=code&redirectUri=http://localhost:9000/callback", a.endPoint, a.appName, a.clientID)

	// 验证和清理输入
	if userName == "" || passWord == "" {
		return "", errors.New("用户名或密码为空")
	}

	// 序列化 JSON 数据并捕获错误
	jsonData := map[string]any{
		"username":     userName,
		"password":     passWord,
		"type":         "code",
		"application":  a.appName,
		"organization": a.orgName,
	}
	jsonStr, err := json.Marshal(jsonData)
	if err != nil {
		return "", fmt.Errorf("JSON 序列化失败: %w", err)
	}

	// 发送 HTTP POST 请求并捕获错误
	resp, err := http.Post(url, "application/json", bytes.NewReader(jsonStr))
	if err != nil {
		return "", fmt.Errorf("HTTP 请求失败: %w", err)
	}
	defer resp.Body.Close()

	// 读取响应体并捕获错误
	bs, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("读取响应体失败: %w", err)
	}

	// 反序列化 JSON 数据并捕获错误
	var mp map[string]any
	if err := json.Unmarshal(bs, &mp); err != nil {
		return "", fmt.Errorf("JSON 反序列化失败: %w", err)
	}

	// 安全地检查状态码
	status, ok := mp["status"].(string)
	if !ok || !strings.EqualFold(status, "OK") {
		msg, _ := mp["msg"].(string) // 忽略类型断言错误，因为已经知道不是 OK
		return "", fmt.Errorf("获取 code 失败: %s", msg)
	}

	// 获取 data 并返回
	code, ok := mp["data"].(string)
	if !ok {
		return "", errors.New("无效的 data 格式")
	}

	return code, nil
}

// Signin 处理用户登录逻辑，包括获取code和解析JWT令牌，并将用户信息保存到session中
//
// 参数:
//
//	userName - 用户名
//	passWord - 密码
//	ctx - Gin框架的上下文，用于管理session
//
// 返回值:
//
//	*casdoorsdk.Claims - 用户声明
//	error - 错误信息，如果出现错误则返回
func (a *Auth) Signin(ctx *gin.Context, userName, passWord string) (*casdoorsdk.Claims, error) {
	// 获取授权码
	code, err := a.getCode(userName, passWord)
	if err != nil {
		return nil, errors.Wrap(err, "获取授权码失败")
	}

	// 生成随机状态值以防止CSRF攻击
	state, err := generateRandomState()
	if err != nil {
		return nil, errors.Wrap(err, "生成随机状态值失败")
	}

	// 使用授权码和状态请求Casdoor SDK以获取OAuth令牌
	token, err := casdoorsdk.GetOAuthToken(code, state)
	if err != nil {
		return nil, errors.Wrap(err, "获取OAuth令牌失败")
	}

	// 解析JWT令牌以获取用户信息
	claims, err := casdoorsdk.ParseJwtToken(token.AccessToken)
	if err != nil {
		return nil, errors.Wrap(err, "解析JWT令牌失败")
	}

	// 更新用户的在线状态
	claims.User.IsOnline = true
	_, err = casdoorsdk.UpdateUserForColumns(&claims.User, []string{"isOnline"})
	if err != nil {
		return nil, errors.Wrap(err, "更新用户在线状态失败")
	}

	// 将访问令牌添加到用户声明中
	claims.AccessToken = token.AccessToken

	// 将用户信息保存到session中
	session := sessions.Default(ctx)
	session.Set("user", claims)
	if err := session.Save(); err != nil {
		return nil, errors.Wrap(err, "保存session失败")
	}

	return claims, nil
}

// 辅助函数：生成随机状态值
func generateRandomState() (string, error) {
	b := make([]byte, 16)
	if _, err := rand.Read(b); err != nil {
		return "", err
	}
	return base64.URLEncoding.EncodeToString(b), nil
}

// GetClaims 检查session中是否存在用户的登录信息
//
// 参数:
//
//	ctx - Gin框架的上下文，用于管理session
//
// 返回值:
//
//	*casdoorsdk.Claims - 用户声明
func (a *Auth) GetClaims(ctx *gin.Context) *casdoorsdk.Claims {
	session := sessions.Default(ctx)
	claimsInterface := session.Get("user")
	if claimsInterface == nil {
		return nil
	}

	// 类型非指针类型，需要类型断言
	claims, ok := claimsInterface.(casdoorsdk.Claims)
	if !ok {
		return nil
	}

	// 避免同一台设备登录多个账号
	headerToker := strings.Replace(ctx.GetHeader("Authorization"), "Bearer ", "", 1) // apiFox 会增加 Bearer
	if headerToker != claims.AccessToken {
		session.Delete("user")
		return nil
	}
	tmp, _ := casdoorsdk.ParseJwtToken(headerToker)
	if tmp.User.Id != claims.User.Id {
		session.Delete("user")
		return nil
	}
	return &claims
}

// Signout 处理用户退出逻辑，包括更新用户的在线状态和清除session
//
// 参数:
//
//	ctx - Gin框架的上下文，用于管理session
//
// 返回值:
//
//	error - 错误信息，如果出现错误则返回
func (a *Auth) Signout(ctx *gin.Context) error {
	// 获取用户信息
	claims := a.GetClaims(ctx)
	if claims == nil {
		return errors.New("用户未登录")
	}

	// 更新用户的在线状态
	claims.User.IsOnline = false
	_, err := casdoorsdk.UpdateUserForColumns(&claims.User, []string{"isOnline"})
	if err != nil {
		return errors.Wrap(err, "更新用户在线状态失败")
	}

	// 清除session中的用户信息
	session := sessions.Default(ctx)
	session.Delete("user")
	if err := session.Save(); err != nil {
		return errors.Wrap(err, "清除session失败")
	}

	return nil
}

// ChangePwd 修改用户的密码。
//
// 参数:
//
//	userName - 用户名
//	oldPassword - 旧密码
//	newPassword - 新密码
//
// 返回值:
//
//	error - 错误信息，如果出现错误则返回
func (a *Auth) ChangePwd(userName, oldPassword, newPassword string) error {
	// 验证和清理输入
	if userName == "" || oldPassword == "" || newPassword == "" {
		return errors.New("用户名、旧密码或新密码为空")
	}

	// 使用casdoorsdk.SetPassword修改密码
	_, err := casdoorsdk.SetPassword(a.orgName, userName, oldPassword, newPassword)
	return err
}

// CheckPolicy 检查当前用户是否有权限执行请求的操作。先执行 InitializeCasbinService 初始化CasbinService，然后调用 GetCasbinService() 获取全局的 CasbinService 实例。
//
// 这个函数通过验证用户的部门和请求的URL路径以及方法来确定用户是否有足够的权限。
// 参数:
//
//	userName - 用户名
//	urlPath - 请求的URL路径
//	action - 请求的操作
//
// 返回值:
//
//	error: 如果用户没有足够的权限或者获取用户部门信息失败，返回相应的错误信息。
func (a *Auth) CheckPolicy(userName, urlPath, action string) error {
	// 检查用户是否具有执行请求操作的权限。
	// 如果用户没有足够的权限，返回权限不足的错误。
	b, err := GetCasbinService().Enforce(userName, a.appName, urlPath, action)
	if err != nil {
		return fmt.Errorf("检查用户权限失败: %s", err.Error())
	}
	if !b {
		return fmt.Errorf("权限不足")
	}

	// 如果一切检查通过，返回nil表示用户有权限执行请求的操作。
	return nil
}

// HdlCheckAuth 是一个中间件函数，用于检查和验证用户的认证信息
//
// 它跳过了对本地测试环境、signin、signout、白名单IP的认证
// 对于已认证的用户，它将用户信息设置到上下文中，以便后续处理
func HdlCheckAuth(auth *Auth) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		// 跳过对登录请求,退出请示的认证, 跳过白名单的 IP
		if strings.Contains(ctx.Request.Host, "localhost") || strings.Contains(ctx.Request.RequestURI, "signin") || strings.Contains(ctx.Request.RequestURI, "signout") || slices.Contains(auth.WhiteList, ctx.GetHeader("X-Real-IP")) || slices.Contains(auth.WhiteList, ctx.ClientIP()) { // 白名单
			ctx.Next()
			return
		}

		// 登录信息验证
		claims := auth.GetClaims(ctx)
		if claims == nil {
			ctx.JSON(http.StatusOK, gin.H{
				"Success": false,
				"Data":    "认证失败",
			})
			// 不调用该请求的剩余处理程序
			ctx.Abort()
			return
		}
		ctx.Next()
	}
}
